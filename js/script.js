$(document).ready(function () {
    $('.slider').slick({
        arrows: false,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        timeout: 2000

    })
})



const card = document.getElementsByClassName('info-card');
for (let i = 0; i < card.length; i++) {
    card[i].addEventListener('click', function () {
        this.classList.toggle('active')
    })
}


const dropCard = document.getElementsByClassName('dropdown-card');
for (let i = 0; i < dropCard.length; i++) {
    dropCard[i].addEventListener('click', function () {
        this.classList.toggle('drop')
    })
}

const btn = document.querySelectorAll('[data-tab]')
const block = document.querySelectorAll('[data-tab-content]')

btn.forEach(function (item) {
    item.addEventListener('click', function () {
        block.forEach(function (item) {
            item.classList.add('hidden');
        });
        const contentBox = document.querySelector('#' + this.dataset.tab);
        contentBox.classList.remove('hidden');
    })
})


const buttons = document.querySelectorAll('.btn-all');
for (let i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener("click", function () {
        let current = document.getElementsByClassName("show");
        current[0].className = current[0].className.replace("show", "");
        this.className += " show";
    });
}








